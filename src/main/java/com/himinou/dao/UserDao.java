package com.himinou.dao;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Repository
public class UserDao {
    //userDetails static list no db connection
    private final static List<UserDetails> APPLICATTION_USERS = Arrays.asList(
            new User(
                    "minou@admin.com",
                    "password",
                    Collections.singleton(new SimpleGrantedAuthority("ROLE_Admin"))
            ),
            new User(
                    "minou@user.com",
                    "password",
                    Collections.singleton(new SimpleGrantedAuthority("ROLE_User"))
            )
    );

    public UserDetails findUserByEmail(String email){
        return  APPLICATTION_USERS
                .stream()
                .filter(u -> u.getUsername().equals(email))
                .findFirst()
                .orElseThrow(() -> new UsernameNotFoundException("No user was found"));
    }
}
